import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    addToCount(state, sumToAdd) {
      state.count += sumToAdd;
    }
  },
  actions: {
    incrementCount(state) {
      state.commit('addToCount', 1);
    }
  }
})
