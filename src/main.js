import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import VueAlertify from 'vue-alertify';

Vue.config.productionTip = false

Vue.use(VueAlertify, {
  notifier: {
    // auto-dismiss wait time (in seconds)
    delay: 5,
    // default position
    position: 'bottom-left',
    // adds a close button to notifier messages
    closeButton: false,
  },
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
