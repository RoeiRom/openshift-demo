FROM ubuntu

RUN mkdir /project
WORKDIR /project

COPY ./ /project

RUN apt-get update && apt-get install -y npm
RUN npm install

CMD ["npm", "run", "serve"]
